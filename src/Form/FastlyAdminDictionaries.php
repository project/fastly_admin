<?php
/**
 * Form for administering Fastly Dictionaries
 * PHP Version 7.2
 */
namespace Drupal\fastly_admin\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\fastly_admin\API\FastlyAPIDict;

/**
 * FastlyAdminDictionaries class extending FormBase.
 */
class FastlyAdminDictionaries extends FormBase {
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'fastly_admin_dictionaries';
  }

  /**
   * Undocumented function
   *
   * @param array $form
   * @param FormStateInterface $form_state
   * @return void
   */
  public function buildForm (array $form, FormStateInterface $form_state, $dictid = NULL, $dictname = NULL) {
    // Get specified dictionary
    $fastly = new FastlyAPIDict();
    if($dictid) {
      $dict_info = $fastly->getDictDetails($dictid, $dictname);
      $dict_details = $fastly->getDictListings($dictid);
    } else {
      drupal_set_message($this->t('No Dictionary ID specified'));
    }

    $markup = "<ul>";
    $markup .= "<li>Created: " . $dict_info['created_at'] . "</li>";
    $markup .= "<li>Last Update: " . $dict_info['updated_at'] . "</li>";
    $markup .= "<li>ID: " . $dict_info['id'] . "</li>";
    $markup .= "</ul>";

    $form['fastly_details']['service_info'] = array(
      '#type' => 'item',
      '#title' => urldecode($dictname),
      '#open' => TRUE,
      '#markup' => $markup,
    );

    $default_rows = \Drupal::state()->get('dict');
    $num_rows = count($dict_details);

    if($num_rows == 0) {
      $num_rows = 1;
    }

    $form['dict_group'] = array (
      '#type' => 'table',
      '#header' => array (
        $this->t('Key'),
        $this->t('Value'),
        $this->t('Date Added'),
        t('DELETE'),
        ''
      ),
      '#tabledrag' => array(
        array(
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'dict-order-weight',
        ),
      ),
      '#prefix' => '<div id="dict-table-wrapper">',
      '#suffix' => '</div>',
    );

    $clickCounter = $form_state->getValue('click_counter');
    // Ensure that there is at least one row.
    if ($clickCounter === NULL) {
      $form_state->setValue('click_counter', $num_rows);
      $clickCounter = $num_rows;
    }

    // Build the table rows and columns.
    for ($row = 0; $row <= $clickCounter; $row++) {

      $form['dict_group'][$row]['key'] = [
        '#type' => 'textfield',
        '#title' => $this
          ->t('Key'),
        '#size' => 25,
        '#maxlength' => 255,
        '#title_display' => 'invisible',
        '#default_value' => $dict_details[$row]['item_key'],
      ];

      $form['dict_group'][$row]['value'] = [
        '#type' => 'textfield',
        '#title' => $this
          ->t('Value'),
        '#size' => 15,
        '#title_display' => 'invisible',
        '#default_value' => $dict_details[$row]['item_value'],
      ];

      $form['dict_group'][$row]['date_added'] = [
        '#type' => 'textfield',
        '#title' => $this
          ->t('Date Added'),
        '#size' => 25,
        '#title_display' => 'invisible',
        '#attributes' => array('readonly' => 'readonly'),
        '#default_value' => $dict_details[$row]['created_at'],
      ];

      $form['dict_group'][$row]['delete'] = [
        '#type' => 'checkbox',
        '#title' => $this
          ->t('DELETE'),
        '#size' => 30,
        '#title_display' => 'invisible',
      ];

      $form['dict_group'][$row]['fastly_dict_id'] = [
        '#type' => 'value',
        '#value' => $dictid,
      ];
  }

  $triggeringElement = $form_state->getTriggeringElement();
  if ($triggeringElement and $triggeringElement['#ajax']['callback'] == '::ajaxAddRow') {
    $clickCounter++;
    $form_state->setValue('click_counter',$clickCounter);
    $form['click_counter'] = array('#type' => 'hidden', '#default_value' => 2, '#value' => $clickCounter);
  } else {
    $form['click_counter'] = array('#type' => 'hidden', '#default_value' => 2);
  }

  $form['addRow'] = array(
    '#type' => 'button',
    '#value' => t('Add a row'),
    '#ajax' => array(
      'callback' =>  '::ajaxAddRow',
      'event' => 'click',
      'wrapper' => 'acl-table-wrapper',
    ),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;

}



  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return mixed
   */
  function ajaxAddRow(array &$form, FormStateInterface $form_state) {

    return $form['acl_group'];
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $fastly = new FastlyAPI();
    $values = $form_state->getValue('dict_group');
   // dpm($form_state);
    $elements = $form_state->getCompleteForm()['dict_group'];
      $ids = array();
      $row_count = 0;
      $total_count = 0;
      foreach ($values as $key => $value) {
        if (($value['value'] != $elements[$total_count]['value']['#default_value']) ||
          ($value['key'] != $elements[$total_count]['key']['#default_value'])) {
          // Update fastly config:
          $fastly->addToDictList($value); //add does both update and add.
        }
        if($value['value'] != "") {
          $row_count++;
          $ids[] .= $value['value'] . "|| " . $value['key'];
          // TODO: Verify IP is valid: if (filter_var($ip, FILTER_VALIDATE_IP))...
          // delete rows marked for deletion:
          if($value['delete'] == 1) {
            $row_count--;
            // if there's a fastly ID we need to delete on fastly too.
            if($value['key'] != "") {
              $fastly->deleteFromDictList($value);
            }
          } else {
            $acl_group[$row_count] = $value;
          }
          if($value['key'] == "") {
            // send to fastly
            $new_entry = $fastly->addToDictList($value); // TODO: Add check here if false let user know of failure
            // Update the ID before saving.
            $value['key'] = $new_entry['id'];
          }
        }

        \Drupal::state()->set('dict_group', $dict_group);

      $total_count++;
    }
    $output = $this
    ->t('Updating Dictionary entries: @ids', [
      '@ids' => implode(', ', $ids),
    ]);
  $this
    ->messenger()
    ->addMessage($output);
  }

}