<?php
/**
 * Form for purging Fastly cache.
 * PHP Version 7.2
 */
namespace Drupal\fastly_admin\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\fastly_admin\API\FastlyAPI;

/**
 * FastlyAdminPurgeCache class extending FormBase.
 */
class FastlyAdminPurgeCache extends FormBase {
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'fastly_admin_purge';
  }

  /**
   * Undocumented function
   *
   * @param array $form
   * @param FormStateInterface $form_state
   * @return void
   */
  public function buildForm (array $form, FormStateInterface $form_state) {

    // for elements to pass to Fastly for purging.
    $form['fastly_admin_purge']['test'] = [
        '#type' => 'value',
        '#default_value' => 'test',
      ];

      $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Purge Cache'),
      );

      return $form;
  }

/**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $fastly = new FastlyAPI;
    $purge = $fastly->purge();
    $output = $this->t('Cache purged. Status: ' . $purge['status']);
    if($purge['status'] != 'ok') {
        $this
        ->messenger()
        ->addError("Error purgin cache.");
    } else {
        $this
        ->messenger()
        ->addMessage($output);
    }
  }
}
