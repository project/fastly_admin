<?php
/**
 * Form for administering Fastly ACL
 * PHP Version 7.2
 */
namespace Drupal\fastly_admin\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\fastly_admin\API\FastlyAPIACL;

/**
 * FastlyAdminACL class extending FormBase.
 */
class FastlyAdminACL extends FormBase {
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'fastly_admin_acl';
  }

  /**
   * Undocumented function
   *
   * @param array $form
   * @param FormStateInterface $form_state
   * @return void
   */
  public function buildForm (array $form, FormStateInterface $form_state, $aclid = NULL, $aclname = NULL) {
    // Get specified ACL
    $fastly = new FastlyAPIACL();
    if($aclid) {
      $acl_info = $fastly->getACLDetails($aclid, $aclname);
      $acl_details = $fastly->getACLListings($aclid);
    } else {
      drupal_set_message($this->t('No ACL ID specified'));
    }

    $markup = "<ul>";
    $markup .= "<li>Created: " . $acl_info['created_at'] . "</li>";
    $markup .= "<li>Last Update: " . $acl_info['updated_at'] . "</li>";
    $markup .= "<li>ID: " . $acl_info['id'] . "</li>";
    $markup .= "</ul>";

    $form['fastly_details']['service_info'] = array(
      '#type' => 'item',
      '#title' => urldecode($aclname),
      '#open' => TRUE,
      '#markup' => $markup,
    );

    $default_rows = \Drupal::state()->get('acl');
    $num_rows = count($acl_details);

    if($num_rows == 0) {
      $num_rows = 1;
    }

    $form['acl_group'] = array (
      '#type' => 'table',
      '#header' => array (
        $this->t('Address'),
        $this->t('Subnet'),
        $this->t('Comment'),
        t('DELETE'),
        '',
        ''
      ),
      '#tabledrag' => array(
        array(
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'acl-order-weight',
        ),
      ),
      '#prefix' => '<div id="acl-table-wrapper">',
      '#suffix' => '</div>',
    );

    $clickCounter = $form_state->getValue('click_counter');
    // Ensure that there is at least one row.
    if ($clickCounter === NULL) {
      $form_state->setValue('click_counter', $num_rows);
      $clickCounter = $num_rows;
    }

    // Build the table rows and columns.
    for ($row = 0; $row <= $clickCounter; $row++) {

      $form['acl_group'][$row]['address'] = [
        '#type' => 'textfield',
        '#title' => $this
          ->t('Address'),
        '#size' => 15,
        '#title_display' => 'invisible',
        '#default_value' => $acl_details[$row]['ip'],
      ];

      $form['acl_group'][$row]['subnet'] = [
        '#type' => 'textfield',
        '#title' => $this
          ->t('Subnet'),
        '#size' => 2,
        '#title_display' => 'invisible',
        '#default_value' => $acl_details[$row]['subnet'],
      ];

      $form['acl_group'][$row]['comment'] = [
        '#type' => 'textfield',
        '#title' => $this
          ->t('Comment'),
        '#size' => 25,
        '#maxlength' => 255,
        '#title_display' => 'invisible',
        '#default_value' => $acl_details[$row]['comment'],
      ];

      $form['acl_group'][$row]['delete'] = [
        '#type' => 'checkbox',
        '#title' => $this
          ->t('DELETE'),
        '#size' => 30,
        '#title_display' => 'invisible',
      ];

      // Hidden fileds for tracking.
      $form['acl_group'][$row]['fastly_item_id'] = [
        '#type' => 'value',
        '#value' => $acl_details[$row]['id'],
      ];

      $form['acl_group'][$row]['fastly_acl_id'] = [
        '#type' => 'value',
        '#value' => $aclid,
      ];

    }

    $triggeringElement = $form_state->getTriggeringElement();
    if ($triggeringElement and $triggeringElement['#ajax']['callback'] == '::ajaxAddRow') {
      $clickCounter++;
      $form_state->setValue('click_counter',$clickCounter);
      $form['click_counter'] = array('#type' => 'hidden', '#default_value' => 2, '#value' => $clickCounter);
    } else {
      $form['click_counter'] = array('#type' => 'hidden', '#default_value' => 2);
    }

    $form['addRow'] = array(
      '#type' => 'button',
      '#value' => t('Add a row'),
      '#ajax' => array(
        'callback' =>  '::ajaxAddRow',
        'event' => 'click',
        'wrapper' => 'acl-table-wrapper',
      ),
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );

    return $form;

  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return mixed
   */
  function ajaxAddRow(array &$form, FormStateInterface $form_state) {

    return $form['acl_group'];
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $fastly = new FastlyAPIACL();
    $values = $form_state->getValue('acl_group');
   // dpm($form_state);
    $elements = $form_state->getCompleteForm()['acl_group'];
      $ids = array();
      $row_count = 0;
      $total_count = 0;
      foreach ($values as $key => $value) {
        if (($value['address'] != $elements[$total_count]['address']['#default_value']) ||
          ($value['comment'] != $elements[$total_count]['comment']['#default_value'])) {
          // Update fastly config:
          $fastly->UpdateToACLList($value);
        }
        if($value['address'] != "") {
          $row_count++;
          $ids[] .= $value['address'] . "|| " . $value['fastly_item_id'];
          // TODO: Verify IP is valid: if (filter_var($ip, FILTER_VALIDATE_IP))...
          // delete rows marked for deletion:
          if($value['delete'] == 1) {
            $row_count--;
            // if there's a fastly ID we need to delete on fastly too.
            if($value['fastly_item_id'] != "") {
              $fastly->deleteFromACLList($value);
            }
          } else {
            $acl_group[$row_count] = $value;
          }
          if($value['fastly_item_id'] == "") {
            // send to fastly
            $new_entry = $fastly->addToACLList($value); // TODO: Add check here if false let user know of failure
            // Update the ID before saving.
            $value['fastly_item_id'] = $new_entry['id'];
          }
        }

        \Drupal::state()->set('acl_group', $acl_group);

      $total_count++;
    }
    $output = $this
    ->t('Updating ACL entries: @ids', [
      '@ids' => implode(', ', $ids),
    ]);
  $this
    ->messenger()
    ->addMessage($output);
  }
}