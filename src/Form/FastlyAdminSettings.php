<?php
/**
 * Form for setting Fastly API Credentials
 * PHP Version 7.2
 */
namespace Drupal\fastly_admin\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\fastly_admin\API\FastlyAPIACL;

/**
 * FastlyAdminSettings class extending FormBase.
 */
class FastlyAdminSettings extends FormBase {
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'fastly_admin_settings';
  }

  public function buildForm (array $form, FormStateInterface $form_state) {

    $form['fastly_admin_endpoint'] = array (
      '#type' => 'textfield',
      '#title' => $this->t('Fastly API Enpoint URL'),
      '#required' => TRUE,
      '#description' => $this->t('API enpoint default: https://api.fastly.com/service/'),
      '#default_value' => \Drupal::state()->get('fastly_admin_endpoint'),
    );

    $form['fastly_admin_key'] = array (
      '#type' => 'textfield',
      '#title' => $this->t('Fastly Key'),
      '#required' => TRUE,
      '#description' => $this->t('API Key'),
      '#default_value' => \Drupal::state()->get('fastly_admin_key'),
      );

      $form['fastly_admin_service_id'] = array (
        '#type' => 'textfield',
        '#title' => $this->t('Fastly Service ID'),
        '#required' => TRUE,
        '#description' => $this->t('API Service ID'),
        '#default_value' => \Drupal::state()->get('fastly_admin_service_id'),
      );

      if(\Drupal::state()->get('fastly_admin_key') != NULL && \Drupal::state()->get('fastly_admin_service_id') != NULL) {
        $versions = array();
        $fastly = new FastlyAPIACL;
        $fastly_versions = $fastly->getServiceDetails();
        $fastly_acls = $fastly->getACLList(\Drupal::state()->get('fastly_current_version'));
        $service_name = $fastly_versions['name'];

        foreach($fastly_versions['versions'] as $fastly_version ) {
          $locked = ($fastly_version['locked'] == 1 && $fastly_version['active'] != 1) ? " - Locked" : NULL;
          $active = ($fastly_version['active'] == 1) ? " - Active" : NULL;
          $comment = ($fastly_version['comment'] != '') ? " - " . $fastly_version['comment'] : NULL;
          $versions[$fastly_version['number']] = $fastly_version['number'] . $comment . $locked . $active;
          if($fastly_version['active'] == 1) {
            $active = $fastly_version['number'];
          }
        }

        $form['fastly_details'] = array(
          '#type' => 'fieldset',
          '#title' => $this->t('Details'),
          '#open' => TRUE,
          '#attributes' => ['id' => 'fastly-details-fieldset-wrapper'],
        );

        $form['fastly_details']['fastly_current_version'] = array(
          '#type' => 'select',
          '#title' => $this->t('Versions'),
          '#options' => $versions,
          '#default_value' => \Drupal::state()->get('fastly_current_version'),
        );

        $markup = "<ul>";
        $markup .= "<li>Active Version: " . $active . "</li>";
        if (\Drupal::state()->get('fastly_admin_service_id') != NULL) {
          $markup .= "<li>Selected Version: " . \Drupal::state()->get('fastly_current_version') . " (for editing)</li>";
        }
        $markup .= "</ul>";
        $markup .= "<h3>Configurations</h3>";

        $markup .= "<h4>" . $this->t('ACL LIST') . "</h4>";
        $markup .= "<ul>";
        foreach($fastly_acls as $fastly_acl) {
          $markup .= "<li><a href='/admin/config/system/fastly-admin/acl/" . $fastly_acl['id'] . "/" .  $fastly_acl['name'] . "'>ACL Config [" . $fastly_acl['name'] . "]</a></li>";
        }
        $markup .= "</ul>";
        $markup .= "<a href='#' class='button'>" . $this->t('Create ACL') . "</a>";
        $markup .= "<h4>" . $this->t('DICTIONARIES LIST') . "</h4>";
        $markup .= "<ul>";
        $markup .= "<li><a href='/admin/config/system/fastly-admin/dictionary/'>Dictionary Config</a></li>";
        $markup .= "</ul>";

        $form['fastly_details']['service_info'] = array(
          '#type' => 'item',
          '#title' => $service_name,
          '#open' => TRUE,
          '#markup' => $markup,
        );
      }

      $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Update'),
      );

      return $form;

  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal::state()->set('fastly_admin_key', $form_state->getValues()['fastly_admin_key']);
    \Drupal::state()->set('fastly_admin_service_id', $form_state->getValues()['fastly_admin_service_id']);
    \Drupal::state()->set('fastly_current_version', $form_state->getValues()['fastly_current_version']);
    $endpoint_suffix = (substr($form_state->getValues()['fastly_admin_endpoint'], -1) != '/') ? "/" : NULL;
    \Drupal::state()->set('fastly_admin_endpoint', $form_state->getValues()['fastly_admin_endpoint'] . $endpoint_suffix);

    drupal_set_message('Fastly Settings Updated'); // TODO: Address this <- as it's depricated.
  }

}
