<?php

namespace Drupal\fastly_admin\Controller;

use Drupal\taxonomy\Entity\Term;
use Drupal\node\Entity\Node;
use Fastly\Adapter\Guzzle\GuzzleAdapter;


/**
 * Defines FastlyAdminTest class.
 */
class FastlyAdminTest {

  /**
   * @param string $type
   */
  public function fastlyAdminTest($type = 'all') {

    $adapter = new GuzzleAdapter(\Drupal::state()->get('fastly_admin_key'));
    //$fastly = new Fastly($adapter);

    //$result = $fastly->send('GET', 'stats?from=1+day+ago');

    //$result = $fastly->purgeAll('some_service_id');

    return array(
        '#markup' => '<p>' . json_encode($result) . '</p>',
      );
  }



}
