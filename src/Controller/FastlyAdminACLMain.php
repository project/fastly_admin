<?php

namespace Drupal\fastly_admin\Controller;

use Drupal\fastly_admin\API\FastlyAPIACL;

/**
 * Defines FastlyAdminACLMain class.
 */
class FastlyAdminACLMain {

  /**
   * @param string $type
   */
  public function fastlyAdminACLMain() {

    // Put this in the API file...
    $fastly = new FastlyAPIACL();
    $fastly_acls = $fastly->getACLList(\Drupal::state()->get('fastly_current_version'));
    $fastly_versions = $fastly->getServiceDetails();

    foreach($fastly_versions['versions'] as $fastly_version ) {
        if($fastly_version['active'] == 1) {
          $active = $fastly_version['number'];
        }
      }
      $markup = "<h2>ACL LIST</h2>";
      $markup .= "<ul>";
      foreach($fastly_acls as $fastly_acl) {
        $markup .= "<li><a href='/admin/config/system/fastly-admin/acl/" . $fastly_acl['id'] . "/" .  $fastly_acl['name'] . "'>ACL Config [" . $fastly_acl['name'] . "]</a></li>";
      }
      $markup .= "</ul>";

    return array(
        '#markup' => $markup,
      );
  }



}
