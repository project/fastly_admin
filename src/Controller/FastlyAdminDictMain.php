<?php

namespace Drupal\fastly_admin\Controller;

use Drupal\fastly_admin\API\FastlyAPIDict;

/**
 * Defines FastlyAdminDictMain class.
 */
class FastlyAdminDictMain {

  /**
   * @param string $type
   */
  public function fastlyAdminDictMain() {

    // Put this in the API file...
    $fastly = new FastlyAPIDict();
    $fastly_dicts = $fastly->getDictList();
    $fastly_versions = $fastly->getServiceDetails();

    foreach($fastly_versions['versions'] as $fastly_version ) {
        if($fastly_version['active'] == 1) {
          $active = $fastly_version['number'];
        }
      }
      $markup = "<h2>Dictionary LIST</h2>";
      $markup .= "<ul>";
      foreach($fastly_dicts as $fastly_dict) {
        $markup .= "<li><a href='/admin/config/system/fastly-admin/dictionaries/" . $fastly_dict['id'] . "/" .  $fastly_dict['name'] . "'>Dictionary Config [" . $fastly_dict['name'] . "]</a></li>";
      }
      $markup .= "</ul>";

    return array(
        '#markup' => $markup,
      );
  }



}
