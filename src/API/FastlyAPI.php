<?php
/**
 * TODO: Write synosis here.
 */
namespace Drupal\fastly_admin\API;

/**
 * Class FastlyAPI
 */
class FastlyAPI {

  public function __construct() {
    // Get APIKey and Session ID for Class
    $this->fastly_api_key = \Drupal::state()->get('fastly_admin_key');
    $this->fastly_service_id = \Drupal::state()->get('fastly_admin_service_id');
    $this->fastly_active_version = \Drupal::state()->get('fastly_current_version');
    $this->fastly_api_endpoint = \Drupal::state()->get('fastly_admin_endpoint');
  }

  /**
   * Undocumented function
   *
   * @return void
   */
  public function getServiceDetails() {
    $endpoint = $this->fastly_api_endpoint . $this->fastly_service_id;
    return self::deliverPayload($endpoint, "GET");
  }

  public function purge() {
    $endpoint = $this->fastly_api_endpoint . $this->fastly_service_id . '/purge_all';
    return self::deliverPayload($endpoint, "POST");
  }

  /**
   * Undocumented function
   *
   * @param [type] $data
   * @return void
   */
  protected function buildEndpointUrl($data) {
    $endpoint = $this->fastly_api_endpoint . $this->fastly_service_id;
    foreach($data as $value) {
      $endpoint .= "/" . $value;
    }
    return $endpoint;
  }

  /**
   * Undocumented function
   *
   * @param [type] $endpoint
   * @param [type] $method
   * @return void
   */
  protected function deliverPayload($endpoint, $method, $payload = NULL) {

    $curl = curl_init($endpoint);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json')); // Accept JSON response
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Fastly-Key: ' . $this->fastly_api_key));

    if($method != 'POST') {
      curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    } else {
      curl_setopt($curl, CURLOPT_POST, 1);
    }
    if($payload != "") {
      curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
    }
    curl_setopt($curl, CURLOPT_HEADER, FALSE);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_FAILONERROR, TRUE);

    $response = curl_exec($curl);
    $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    // Check if login was successful
    if ($http_code == 200) {
      $return = json_decode($response, TRUE);
      if(isset($return)) {
        return $return;
      } //TODDO: What to return if no $return value.

    } else {
     // drupal_set_message(t('Error: ') . $http_code, 'error');
      //return FALSE;
      return curl_error($curl);
    }
  }

}