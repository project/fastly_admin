<?php
/**
 * TODO: Write synosis here.
 */
namespace Drupal\fastly_admin\API;

/**
 * Class FastlyAPIACL
 */
class FastlyAPIACL extends FastlyAPI {

    public function getACLList() {
        $endpoint = $this->fastly_api_endpoint . $this->fastly_service_id . "/version/" . $this->fastly_active_version . "/acl";
        return self::deliverPayload($endpoint, "GET");
      }

      public function getACLDetails($acl_id, $acl_name) {
        $endpoint = $this->fastly_api_endpoint . $this->fastly_service_id . "/version/" . $this->fastly_active_version . "/acl/" . rawurlencode($acl_name);
        return self::deliverPayload($endpoint, "GET", NULL);
      }

      public function getACLListings($acl_id) {
        $endpoint = $this->fastly_api_endpoint . $this->fastly_service_id . "/acl/" . $acl_id . "/entries";
        return self::deliverPayload($endpoint, "GET", NULL);
      }

      public function addToACLList($data) {
        $endpoint = $this->fastly_api_endpoint . $this->fastly_service_id . "/acl/" . $data['fastly_acl_id'] . "/entry";
        $payload = "ip=" .$data['address'] ."&subnet=" . $data['subnet'] . "&negated=0&comment=" . $data['comment'];
        return self::deliverPayload($endpoint, "POST", $payload);
      }

      public function deleteFromACLList($data) {
        $endpoint = $this->fastly_api_endpoint . $this->fastly_service_id . "/acl/" . $data['fastly_acl_id'] . "/entry/" . $data['fastly_item_id'];
        return self::deliverPayload($endpoint, "DELETE", NULL);
      }

      public function UpdateToACLList($data) {
        $endpoint = $this->fastly_api_endpoint . $this->fastly_service_id . "/acl/" . $data['fastly_acl_id'] . "/entry/" . $data['fastly_item_id'];
        $payload = "ip=" .$data['address'] ."&subnet=" . $data['subnet'] . "&negated=0&comment=" . $data['comment'];
        return self::deliverPayload($endpoint, "PATCH", $payload);
      }

}

