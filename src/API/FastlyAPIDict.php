<?php
/**
 * TODO: Write synosis here.
 */
namespace Drupal\fastly_admin\API;

/**
 * Class FastlyAPIACL
 */
class FastlyAPIDict extends FastlyAPI {


  public function getDictList() {
    $endpoint = $this->fastly_api_endpoint . $this->fastly_service_id . "/version/" . $this->fastly_active_version . "/dictionary";
    return self::deliverPayload($endpoint, "GET");
  }

  public function getDictDetails($dict_id, $dict_name) {
    $endpoint = $this->fastly_api_endpoint . $this->fastly_service_id . "/version/" . $this->fastly_active_version . "/dictionary/" . rawurlencode($dict_name);
    return self::deliverPayload($endpoint, "GET", NULL);
  }

  public function getDictListings($dict_id) {
    $endpoint = $this->fastly_api_endpoint . $this->fastly_service_id . "/dictionary/" . $dict_id . "/items";
    return self::deliverPayload($endpoint, "GET", NULL);
  }

  public function addToDictList($data) {
    $endpoint = $this->fastly_api_endpoint . $this->fastly_service_id . "/dictionary/" . $data['fastly_dict_id'] . "/item/" . $data['key'];
    $payload = "item_value=" . $data['value'];
    return self::deliverPayload($endpoint, "PUT", $payload);
  }

  public function deleteFromDictList($data) {
    $elements = array(
      'type' => 'dictionary',
      'id' => $data['fastly_dict_id'],
      'elemental_id' => 'item',
      'elemental_id_value' => $data['key']
    );
    $endpoint = self::buildEndpointUrl($elements);
    return self::deliverPayload($endpoint, "DELETE", NULL);
  }

}